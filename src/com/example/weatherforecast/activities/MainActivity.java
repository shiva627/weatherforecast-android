package com.example.weatherforecast.activities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.weatherforecast.R;
import com.example.weatherforecast.fragments.TaskFragment;
import com.example.weatherforecast.helpers.MyLocationFinder;

public class MainActivity extends NetworkAwareActivity {

	private Fragment mFragment;
	private Button mFetchWeather;
	private EditText mZipcode;
	private ProgressDialog pDialog;
	private ListView mListForecast;
	private TextView mUseMyLocation;
	private MyLocationFinder mLocationFinder;
	private SharedPreferences mSharedPreferences;
	private List<String> mResultList;
	private String mRegex = "^[0-9]{5}(?:-[0-9]{4})?$";
	private Pattern pattern = Pattern.compile(mRegex);
	private static final String MY_PREFERENCES = "MyPrefs" ;
	private static final String MY_FRAGMENT = "WeatherFragment";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mFetchWeather = (Button)findViewById(R.id.btnFetchWeather);
		mZipcode = (EditText)findViewById(R.id.etZipCode);
		mListForecast = (ListView)findViewById(R.id.lvWeatherForecast);
		mUseMyLocation = (TextView)findViewById(R.id.tvUseMyLocation);
		mLocationFinder = new MyLocationFinder(this);

		mSharedPreferences = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);

		//Task Fragment for first oncreate() and subsequent oncreate()
		if(savedInstanceState==null){
			mFragment = new TaskFragment();
			getSupportFragmentManager().beginTransaction().add(mFragment, MY_FRAGMENT).commit();
		}else{
			mFragment = (TaskFragment)getSupportFragmentManager().findFragmentByTag(MY_FRAGMENT);
		}

		mUseMyLocation.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String zipcode = mLocationFinder.getPostalCode();
				if(zipcode == null){
					Toast.makeText(getApplicationContext(), getString(R.string.location_not_found), Toast.LENGTH_SHORT).show();
					//Reconnect to Play Services
					mLocationFinder.connectFinder();
				}else{
					mZipcode.setText(zipcode);
				}
			}
		});

		mFetchWeather.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				//Check network connection
				int networkStatus = NetworkUtil.getConnectivityStatus(getApplicationContext());
				if(networkStatus != 0) {
					String zipcode = mZipcode.getText().toString();
					Matcher matcher = pattern.matcher(zipcode);
					if(matcher.matches()){
						((TaskFragment) mFragment).beginWeatherTask(zipcode);
					}else{
						Toast.makeText(MainActivity.this, getString(R.string.zipcode_not_valid), Toast.LENGTH_SHORT).show();
					}
				}else{
					Toast.makeText(MainActivity.this, getString(R.string.no_network), Toast.LENGTH_SHORT).show();
				}
			}
		});

		if(mFragment != null){
			if(((TaskFragment) mFragment).fetchWeatherTask != null && 
					((TaskFragment) mFragment).fetchWeatherTask.getStatus() == android.os.AsyncTask.Status.RUNNING){
				pDialog = new ProgressDialog(MainActivity.this);
				pDialog.setMessage("Fetching Data ...");
				pDialog.setCancelable(false);
				pDialog.show();
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar items here
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Toast.makeText(MainActivity.this, getString(R.string.settings_here), Toast.LENGTH_SHORT).show();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onStart() {
		super.onStart();
		mLocationFinder.connectFinder();
		loadSharedPreferences();
	}

	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onStop() {
		super.onStop();
		mLocationFinder.disconnectFinder();
		saveSharedPreferences();
	}

	/**
	 * Saves info on activity rotation/close
	 */
	private void saveSharedPreferences() {
		SharedPreferences.Editor editor = mSharedPreferences.edit();

		editor.putString(getString(R.string.zipcode), mZipcode.getText().toString());
		if (mResultList != null) {
			//Set the values
			Set<String> set = new HashSet<String>();
			set.addAll(mResultList);
			editor.putStringSet("Result", set);
		}
		editor.commit();
	}

	/**
	 * Loads info on activity rotation/open
	 */
	private void loadSharedPreferences() {
		mZipcode.setText(mSharedPreferences.getString(getString(R.string.zipcode), getString(R.string.zipcode)));
		//Retrieve the values
		Set<String> set = mSharedPreferences.getStringSet("Result", null);
		if (set != null) {
			set = new TreeSet<String>(set);
			List<String> result = new ArrayList<String>(set);
			setListAdapter(result);
		}
	}

	/**
	 * Shows dialog when download starts 
	 */
	public void showProgressDialogBeforeDownload(){
		if(((TaskFragment) mFragment).fetchWeatherTask != null){
			pDialog = new ProgressDialog(MainActivity.this);
			pDialog.setMessage("Fetching Data ...");
			pDialog.setCancelable(false);
			pDialog.show();
		}
	}

	/**
	 * Dismisses dialog
	 */
	public void dismissDialog(){
		if (pDialog != null){
			pDialog.dismiss();
		}
	}

	/**
	 * Hides dialog if being displayed
	 */
	public void hideProgressDialogAfterDownload(){
		if(((TaskFragment) mFragment).fetchWeatherTask != null){
			if(pDialog.isShowing()){
				pDialog.dismiss();
			}
		}
	}

	/**
	 * Displays weather info to the user
	 * 
	 * @param result the weather info as list
	 */
	public void displayResult(ArrayList<String> result){
		if(result.get(0).equals("Error")){
			Toast.makeText(MainActivity.this, "Oops!! Not a valid zipcode!", Toast.LENGTH_SHORT).show();
			mListForecast.setAdapter(null);
		}else if(result.get(0).equals("ErrorUnableToFetch")){
			Toast.makeText(MainActivity.this, "Sorry! Unable to forecast for this location!", Toast.LENGTH_SHORT).show();
			mListForecast.setAdapter(null);
		}else{
			mResultList = result;
			setListAdapter(mResultList);
		}
	}

	/**
	 * Sets the list to the adapter
	 * 
	 * @param result result list
	 */
	private void setListAdapter(List<String> result) {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, result);
		mListForecast.setAdapter(adapter);
		mListForecast.setBackgroundColor(Color.BLACK);
	}

}