package com.example.weatherforecast.fragments;


import com.example.weatherforecast.helpers.FetchWeatherTask;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * TaskFragment whose instance is retained across activity recreation. It starts the asynctask
 * @author ShivaKrishna
 *
 */
public class TaskFragment extends Fragment{

	private Activity activity;
	public FetchWeatherTask fetchWeatherTask;

	public TaskFragment(){
	}

	/**
	 * starts the asynctask and sends the activity which is calling it 
	 * @param params zipcode for which weather is forecasted
	 */
	public void beginWeatherTask(String... params){
		fetchWeatherTask = new FetchWeatherTask(activity);
		fetchWeatherTask.execute(params);
	}

	/**
	 * onAttach called when fragments instance created
	 */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.activity = activity;
		if(fetchWeatherTask!=null){
			fetchWeatherTask.onAttach(activity);
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return null;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		//Fragment instance is retained across activity recreation
		setRetainInstance(true);
	}

	/**
	 * onDetach called when the activity is destroyed
	 */
	@Override
	public void onDetach() {
		super.onDetach();
		if(fetchWeatherTask!=null){
			fetchWeatherTask.onDetach();
		}
	}
}
