package com.example.weatherforecast.helpers;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.weatherforecast.activities.MainActivity;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

/**
 * AynscTask which requests the data from wunderground apis and sends it back to activity for display
 * @author ShivaKrishna
 *
 */
public class FetchWeatherTask extends AsyncTask<String, Void, ArrayList<String>>{

	private Activity activity;
	private static final String CITY_STATE_URL = "http://ziptasticapi.com/";
	private static final String WEATHER_URL = "http://api.wunderground.com/api/0445a1a8bdc91f9f/forecast/q/";
	private static String zipcode, CITY_STATE_FINAL_URL, WEATHER_FINAL_URL;
	public static final String TAG_ERROR = "error";
	public static final String TAG_COUNTRY = "country";
	public static final String TAG_STATE = "state";
	public static final String TAG_CITY = "city";
	public static final String TAG_FORECAST = "forecast";
	public static final String TAG_SIMPLE_FORECAST = "simpleforecast";
	public static final String TAG_FORECAST_DAY = "forecastday";
	public static final String TAG_PRETTY = "pretty";
	public static final String TAG_HIGH = "high";
	public static final String TAG_LOW = "low";
	public static final String TAG_CONDITIONS = "conditions";
	public static final String TAG_DATE = "date";
	public static final String TAG_FAHRENHEIT = "fahrenheit";
	public static final String TAG_CELSIUS = "celsius";
	public static final String TAG_RESPONSE = "response";
	public final String DEGREE  = "\u00b0";

	ArrayList<String> mWeatherResponse = new ArrayList<String>();

	public FetchWeatherTask(Activity activity){
		onAttach(activity);
	}

	public void onAttach(Activity activity){
		this.activity = activity;
	}

	/**
	 * sets the activity to null when activity is destroyed
	 */
	public void onDetach(){
		((MainActivity)activity).dismissDialog();
		activity = null;
	}

	@Override
	protected void onPreExecute() {
		if(activity!=null){
			((MainActivity)activity).showProgressDialogBeforeDownload();
		}
	}

	@Override
	protected ArrayList<String> doInBackground(String... params) {
		zipcode = params[0].substring(0, 5);
		CITY_STATE_FINAL_URL = CITY_STATE_URL+zipcode;
		try{
			String cityState = getJSONfromURL(CITY_STATE_FINAL_URL);
			JSONObject cityStateJsonObject = new JSONObject(cityState);
			if(cityStateJsonObject.has(TAG_ERROR)){
				//				Log.d("Error", "Error");
				// Stop here and return not a valid zipcode msg to user
				mWeatherResponse.add("Error");
			}else{
				String city = cityStateJsonObject.getString(TAG_CITY);
				String state = cityStateJsonObject.getString(TAG_STATE);

				WEATHER_FINAL_URL = WEATHER_URL+state+"/"+city.replaceAll(" ", "_")+".json";
				String weather = getJSONfromURL(WEATHER_FINAL_URL);
				JSONObject weatherJsonArray = new JSONObject(weather);
				JSONObject responseObject = weatherJsonArray.getJSONObject(TAG_RESPONSE);
				if(responseObject.has(TAG_ERROR)){
					// Stop here and return not a valid zipcode msg to user
					mWeatherResponse.clear();
					mWeatherResponse.add("ErrorUnableToFetch");
				}else{	
					JSONObject forecast = weatherJsonArray.getJSONObject(TAG_FORECAST);
					JSONObject simpleForecast = forecast.getJSONObject(TAG_SIMPLE_FORECAST);
					JSONArray forecastDay = simpleForecast.getJSONArray(TAG_FORECAST_DAY);

					JSONObject forecastDayObject1 = forecastDay.getJSONObject(0);
					JSONObject date = forecastDayObject1.getJSONObject(TAG_DATE);
					String prettyDate = date.getString(TAG_PRETTY);

					JSONObject high = forecastDayObject1.getJSONObject(TAG_HIGH);
					JSONObject low = forecastDayObject1.getJSONObject(TAG_LOW);

					String highFahrenheit = high.getString(TAG_FAHRENHEIT);
					String highCelsius = high.getString(TAG_CELSIUS);

					String lowfahrenheit = low.getString(TAG_FAHRENHEIT);
					String lowCelsius = low.getString(TAG_CELSIUS);

					String conditions =  forecastDayObject1.getString(TAG_CONDITIONS);

					mWeatherResponse.add("A) LOCATION : " + city + ", " + state);
					mWeatherResponse.add("B) DATE : " + prettyDate);
					mWeatherResponse.add("C) CONDITIONS : " + conditions);
					mWeatherResponse.add("D) LOW : " + lowfahrenheit + DEGREE + "F / " + lowCelsius + DEGREE + "C");
					mWeatherResponse.add("E) HIGH : " + highFahrenheit + DEGREE + "F / " + highCelsius + DEGREE + "C");
					
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return mWeatherResponse;
	}

	@Override
	protected void onPostExecute(ArrayList<String> result) {
		if(activity!=null){
			((MainActivity)activity).hideProgressDialogAfterDownload();
			((MainActivity)activity).displayResult(result);
		}
	}

	public static String getJSONfromURL(String url){

		InputStream is = null;
		String result = "";
		JSONObject jsonObj = null;
		JSONArray jsonArray = null;
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httppost = new HttpGet(url);
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		}catch(Exception e){
			Log.e("log_tag", "Error in http connection "+e.toString());
		}
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result=sb.toString();
			//			Log.d("check json", ""+result);
		}catch(Exception e){
			Log.e("log_tag", "Error converting result "+e.toString());
		}
		return result;
	}
}