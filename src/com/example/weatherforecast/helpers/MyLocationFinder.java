package com.example.weatherforecast.helpers;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * MyLocationFinder to find the users location Zipcode
 * @author ShivaKrishna
 *
 */
public class MyLocationFinder implements
ConnectionCallbacks, OnConnectionFailedListener {

	protected static final String TAG = "MyLocationFinder";
	/**
	 * Provides the entry point to Google Play services.
	 */
	protected GoogleApiClient mGoogleApiClient;
	/**
	 * Represents a geographical location.
	 */
	protected Location mLastLocation;
	protected Context mContext;

	private String mPostalCode;

	public MyLocationFinder(Context context) {
		mContext = context;
		buildGoogleApiClient();
	}

	/**
	 * Gets the postal code if location is enabled, else returns null. Call only after a connection is established.
	 *
	 * @return postal code
	 */
	public String getPostalCode() {
		return mPostalCode;
	}

	/**
	 * Builds a GoogleApiClient. Uses the addApi() method to request the LocationServices API.
	 */
	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(mContext)
		.addConnectionCallbacks(this)
		.addOnConnectionFailedListener(this)
		.addApi(LocationServices.API)
		.build();
	}

	/**
	 * Connects Google play services
	 */
	public void connectFinder() {
		mGoogleApiClient.connect();
	}

	/**
	 * Disconnects Google play services
	 */
	public void disconnectFinder() {
		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}
	}

	/**
	 * Runs when a GoogleApiClient object successfully connects
	 */
	@Override
	public void onConnected(Bundle connectionHint) {
		// Gets the last known location
		mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
		if (mLastLocation != null) {

			double latitude = mLastLocation.getLatitude();
			double longitude = mLastLocation.getLongitude();

			Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
			// latitude,longitude has current location
			try {
				List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 3);

				for (Address adr: addresses) {
					if (adr.getPostalCode() != null) {
						mPostalCode = adr.getPostalCode();
						break;
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// Log the error code
		Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
	}


	@Override
	public void onConnectionSuspended(int cause) {
		// Attempt to re-establish connection
		Log.i(TAG, "Connection suspended");
		mGoogleApiClient.connect();
	}


}
